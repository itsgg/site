---
layout: default
---

# Ganesh Gunasegaran

### _Software architect_ with a passion for _revolutionizing online education_

Polyglot programmer/full stack developer, who believes in choosing the right tool for the right job.

Founder of [Akshi](http://akshi.com), a next generation social learning platform.

_"A little better, everyday..."_
